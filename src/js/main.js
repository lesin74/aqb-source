'use strict';
import { sliders } from './sliders';

const body = document.querySelector('body');
const nav = document.querySelector('#nav');
const videoPopup = document.querySelector('.header__board_video');
const headerPointer = document.querySelector('.header__pointer');
const youtubeVideo = document.querySelector('.header__iframe');
let div = document.createElement('div');
const tabsTitleItem = document.querySelectorAll('.tabs__title_item');
const tabsContent = document.querySelectorAll('.tabs__content');
const navHeight = 76;
const leftInfo = document.querySelector('.left__info');
const rightInfo = document.querySelector('.right__info');
const infoContent = document.querySelector('.info__content > img');
const helpBg = document.querySelector('.help');
const started = document.querySelector('.started');
const tabsSelectItem = document.querySelector('.tabs__select_item');
const levelTabsTitle = document.querySelectorAll('.level__tabs_title');
const levelTabsItem = document.querySelectorAll('.level__tabs_item');
const tabsBlockTitle = document.querySelectorAll('.tabs__block_title');
const tabsBlockContent = document.querySelectorAll('.tabs__block_content');

//смена бэграунда хедера при скролле
if (headerPointer) {
    window.addEventListener('scroll', (event) => {
        if (navHeight >= headerPointer.getBoundingClientRect().top) {
            nav.classList.add('nav__bg--active');
        } else {
            nav.classList.remove('nav__bg--active');
        }
    });
} else {
    nav.classList.add('nav__bg--active');
}

//табы на странице Active Query Builder for End Users
for (let i = 0; i < levelTabsTitle.length; i++) {
    if (levelTabsItem[i].classList.contains('level__tabs_item--active')) {
        levelTabsItem[i].style.maxHeight = levelTabsItem[i].scrollHeight + 'px';
    }
    levelTabsTitle[i].addEventListener('click', (title) => {
        levelTabsTitle[i].classList.toggle('level__tabs_title--active');
        levelTabsItem[i].classList.toggle('level__tabs_item--active');
        if (levelTabsItem[i].classList.contains('level__tabs_item--active')) {
            levelTabsItem[i].style.maxHeight = levelTabsItem[i].scrollHeight + 'px';
        } else {
            levelTabsItem[i].style.maxHeight = null;
        }
    });
}

sliders();

if (videoPopup) {
    videoPopup.addEventListener('click', (event) => {
        body.appendChild(div);
        div.classList.add('video__bg');
        youtubeVideo.classList.add('header__frame--active');
        if (div) {
            div.addEventListener('click', (e) => {
                youtubeVideo.classList.remove('header__frame--active');
                div.remove();
            });
        }
    });
}

tabsTitleItem.forEach((item) => {
    item.addEventListener('click', (event) => {
        tabsTitleItem.forEach((e) => {
            e.classList.remove('tabs__title_item--active');
        });
        item.classList.add('tabs__title_item--active');
        tabsContent.forEach((content) => {
            if (item.dataset.tabs == content.dataset.tabs) {
                tabsContent.forEach((e) => {
                    e.classList.remove('tabs__content--active');
                });
                content.classList.add('tabs__content--active');
            }
        });
    });
});

if (tabsSelectItem) {
    tabsSelectItem.addEventListener('change', (event) => {
        tabsContent.forEach((content) => {
            if (tabsSelectItem.value == content.dataset.tabs) {
                tabsContent.forEach((e) => {
                    e.classList.remove('tabs__content--active');
                });
                content.classList.add('tabs__content--active');
            }
        });
    });
}

for (let i = 0; i < tabsBlockTitle.length; i++) {
    tabsBlockTitle[i].addEventListener('click', () => {
        tabsBlockContent[i].classList.toggle('tabs__block_content--active');
        tabsBlockTitle[i].classList.toggle('tabs__block_title--open');
    });
}

function positionBlock() {
    let width = window.innerWidth;
    switch (true) {
        case width < 960 && width > 639:
            if (leftInfo || rightInfo) {
                leftInfo.style.left = infoContent.getBoundingClientRect().left + 'px';
                rightInfo.style.right = infoContent.getBoundingClientRect().x + 28 + 'px';
            }
            if (helpBg) {
                helpBg.classList.remove('help__mobile');
            }

            if (started) {
                started.classList.add('started__mobile');
            }
            break;

        case width < 639 && width > 479:
            if (leftInfo || rightInfo) {
                leftInfo.style.left = infoContent.getBoundingClientRect().left - 10 + 'px';
                rightInfo.style.right = infoContent.getBoundingClientRect().x + 10 + 'px';
            }
            if (helpBg) {
                helpBg.classList.add('help__mobile');
            }
            if (started) {
                started.classList.add('started__mobile');
            }
            break;

        case width < 479:
            if (helpBg) {
                helpBg.classList.add('help__mobile');
            }
            if (started) {
                started.classList.add('started__mobile');
            }
            break;

        default:
            if (leftInfo || rightInfo) {
                leftInfo.style.left = 20 + 'px';
                rightInfo.style.right = 20 + 'px';
            }
    }
}

positionBlock();

window.addEventListener('resize', (event) => {
    positionBlock();
});
