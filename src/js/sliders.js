'use strict';

import swiper, { Navigation, Pagination } from 'swiper/swiper-bundle';

swiper.use([Navigation, Pagination]);

export function sliders() {
    const offersSlider = new swiper('.swiper', {
        loop: true,
        slidesPerView: 1,
        autoplay: {
            delay: 3500,
            disableOnInteraction: false,
        },
        navigation: {
            nextEl: '.offers__slider-button-next',
            prevEl: '.offers__slider-button-prev',
        },
    });

    const usersSlider = new swiper('.swiper__users', {
        slidesPerView: 1,
        navigation: {
            nextEl: '.users__slider-button-next',
            prevEl: '.users__slider-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    });
}
