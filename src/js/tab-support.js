'use strict';

const supportAreaTab = document.querySelectorAll('.support__area_tab');
const supportTabContent = document.querySelectorAll('.support__tab_content');

supportAreaTab.forEach((elem) => {
    elem.addEventListener('click', (event) => {
        supportTabContent.forEach((tabs) => {
            if (elem.dataset.suptab == tabs.dataset.suptab) {
                supportAreaTab.forEach((e) => {
                    e.classList.remove('support__area_tab--active');
                });
                elem.classList.add('support__area_tab--active');

                supportTabContent.forEach((e) => {
                    e.classList.remove('support__tab_content--active');
                });
                tabs.classList.add('support__tab_content--active');
            }
        });
    });
});

//селект на странице support-area
const requiests = [
    { label: 'Bug reports, "How to" questions, Suggestions, Kudos' },
    { label: 'Pre-sales, Licensing, Quotation, Payment questions' },
    { label: '"I am wondering..." (Just a question)' },
    { label: 'Request a quote' },
];

const dropdownWrapper = document.querySelector('.dropdwn__wrapper');
const dropdownItemLabel = document.querySelector('.dropdwn__name_request');
const dropdownItems = document.querySelector('.dropdwn__items');

const dropdownDiv = requiests
    .map((i) => {
        return `<div class="dropdwn__item">${i.label}</div>`;
    })
    .join(' ');

dropdownItems.insertAdjacentHTML('afterbegin', dropdownDiv);

const dropdownItem = document.querySelectorAll('.dropdwn__item');
dropdownWrapper.addEventListener('click', (event) => {
    dropdownItems.classList.toggle('dropdwn__items--active');
    dropdownWrapper.classList.toggle('dropdwn__wrapper--active');

    dropdownItem.forEach((elem) => {
        elem.classList.remove('dropdwn__item--active');
        if (dropdownItemLabel.innerHTML === elem.innerHTML) {
            elem.classList.add('dropdwn__item--active');
        }
    });
});

dropdownItem.forEach((item) => {
    item.addEventListener('click', (event) => {
        dropdownItemLabel.innerHTML = item.innerHTML;
        dropdownItems.classList.remove('dropdwn__items--active');
        dropdownWrapper.classList.remove('dropdwn__wrapper--active');
    });
});

//загрузка файлов на странице support-area

const fileUploader = document.getElementById('support__uploader');

fileUploader.addEventListener('change', (event) => {
    const files = event.target.files;
    for (const file of files) {
        const name = file.name;
        const supportWrapper = document.querySelectorAll('.support__wrapper');
        const feedback = document.querySelector('.support__feedback');
        const msg = `Скриншот ${name} <br/>`;
        supportWrapper.forEach((file) => {
            feedback.innerHTML += `
                <span class="support__feedback_scr">${msg} <img src="../img/src-delete.png" class="support__feedback_del" /></span>
            `;
        });
        const supportFeedbackScr = document.querySelectorAll('.support__feedback_scr');
        const supportFeedbackDel = document.querySelectorAll('.support__feedback_del');
        for (let i = 0; i < supportFeedbackDel.length; i++) {
            supportFeedbackDel[i].addEventListener('click', (event) => {
                supportFeedbackScr[i].remove();
            });
        }
    }
});
